import sirv from "sirv";
import express from "express";
import * as sapper from "@sapper/server";

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === "development";

const app = express()
	.use(sirv("static", { dev }), sapper.middleware())
	.listen(PORT, (err) => {
		if (err) console.log("Error: ", err);
	});

export default app;
